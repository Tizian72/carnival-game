using UnityEngine;

public class ObjectMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float despawnTime = 5f;

    private bool moveRight = true; // Indicador de direcci�n

    private void Update()
    {
        if (moveRight)
        {
            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            // Aumentar el puntaje del jugador correspondiente
            int playerNumber = collision.gameObject.GetComponent<Bullet>().playerNumber;
            GameManager.Instance.AddScore(playerNumber, 1);

            Destroy(gameObject);
        }
    }

    private void Start()
    {
        Invoke("DestroyObject", despawnTime);
    }

    private void DestroyObject()
    {
        Destroy(gameObject);
    }

    public void SetMoveDirection(Vector3 direction)
    {
        moveRight = (direction.x >= 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ChangeDirection"))
        {
            moveRight = !moveRight;
        }
    }
}