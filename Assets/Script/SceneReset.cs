using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneReset : MonoBehaviour
{
    public string sceneName; // Nombre de la escena a reiniciar

    public GameManager gameManager; // Referencia al GameManager

    private void Start()
    {
        if (gameManager == null)
        {
            // Buscar el objeto GameManager en la escena
            gameManager = GameObject.FindObjectOfType<GameManager>();
        }
    }

    private void Update()
    {
        // Reiniciar la escena al presionar la tecla R
        if (Input.GetKeyDown(KeyCode.R))
        {
            gameManager.ResetPlayerPositions();
            SceneManager.LoadScene(sceneName);
        }
    }
}







