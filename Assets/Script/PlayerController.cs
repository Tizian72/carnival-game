using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 5f;
    public KeyCode moveLeftKey;
    public KeyCode moveRightKey;
    public KeyCode jumpKey;
    public KeyCode shootKey;
    public GameObject bulletPrefab;
    public Transform bulletSpawnPoint;
    public float shootCooldown = 0.5f; // Tiempo de espera entre disparos
    private float shootTimer = 0f; // Temporizador para el cooldown
    public float jumpForce = 5f;

    private void Update()
    {
        // Movimiento del jugador
        float moveHorizontal = 0f;
        if (Input.GetKey(moveLeftKey))
        {
            moveHorizontal = -1f;
        }
        else if (Input.GetKey(moveRightKey))
        {
            moveHorizontal = 1f;
        }
        transform.Translate(Vector3.right * moveHorizontal * moveSpeed * Time.deltaTime);

        // Control del cooldown de disparo
        shootTimer -= Time.deltaTime;

        // Disparo del jugador si el cooldown ha terminado
        if (Input.GetKeyDown(shootKey) && shootTimer <= 0f)
        {
            Fire();
            shootTimer = shootCooldown; // Reiniciar el cooldown
        }

        // Salto del jugador
        if (Input.GetKeyDown(jumpKey))
        {
            Jump();
        }
    }

    private void Fire()
    {
        Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
    }

    private void Jump()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
}
