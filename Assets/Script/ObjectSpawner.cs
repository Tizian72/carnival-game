using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject objectPrefab;
    public float spawnInterval = 3f;
    public float objectSpeed = 2f;
    public Vector3 spawnDirection = Vector3.right; // Direcci�n de spawn por defecto

    private void Start()
    {
        // Iniciar el proceso de spawn de objetos
        InvokeRepeating("SpawnObject", 0f, spawnInterval);
    }

    private void SpawnObject()
    {
        // Instanciar un nuevo objeto
        GameObject newObject = Instantiate(objectPrefab, transform.position, Quaternion.identity);

        // Configurar la velocidad y direcci�n del objeto
        ObjectMovement objectMovement = newObject.GetComponent<ObjectMovement>();
        objectMovement.moveSpeed = objectSpeed;
        objectMovement.SetMoveDirection(spawnDirection);
    }
}