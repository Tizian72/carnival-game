using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int playerNumber; // N�mero de jugador asociado a la bala
    public float moveSpeed = 10f;
    public float despawnTime = 3f;

    private void Start()
    {
        // Destruir la bala despu�s de un tiempo de vida
        Destroy(gameObject, despawnTime);
    }

    private void Update()
    {
        // Mover la bala hacia adelante
        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Verificar si la bala ha colisionado con un objetivo
        if (collision.gameObject.CompareTag("Object"))
        {
            Destroy(gameObject); // Destruir la bala al colisionar con un objetivo
        }
    }
}
