using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public GameObject player1;
    public GameObject player2;
    public GameObject spawner;
    public float gameTime = 60f; // Duraci�n del juego en segundos

    public Text player1ScoreText;
    public Text player2ScoreText;
    public Text timerText;
    public Text gameOverText;

    private float timer;
    private bool gamePaused = false;
    private bool gameEnded = false;

    private int player1Score = 0;
    private int player2Score = 0;

    private Vector3 player1StartPosition;
    private Vector3 player2StartPosition;
    private Vector3 spawnerStartPosition;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        player1StartPosition = player1.transform.position;
        player2StartPosition = player2.transform.position;
        spawnerStartPosition = spawner.transform.position;

        timer = gameTime;
        UpdateScoreTexts();
        UpdateTimerText();

        ResumeGame();
    }

    private void Update()
    {
        if (!gamePaused && !gameEnded)
        {
            timer -= Time.deltaTime;
            UpdateTimerText();

            if (timer <= 0f)
            {
                EndGame();
            }
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (gamePaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    public void GameOver()
    {
        PauseGame();
        gameEnded = true;

        string winner = "";
        int maxScore = Mathf.Max(player1Score, player2Score);

        if (maxScore == player1Score)
        {
            winner = "Jugador 1";
        }
        else if (maxScore == player2Score)
        {
            winner = "Jugador 2";
        }
        else
        {
            winner = "Empate";
        }

        if (gameOverText != null)
        {
            gameOverText.text = "�El ganador es: " + winner + "! Puntaje: " + maxScore;
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            Debug.LogWarning("gameOverText is null!");
        }

        if (player1ScoreText != null)
        {
            player1ScoreText.gameObject.SetActive(false);
        }

        if (player2ScoreText != null)
        {
            player2ScoreText.gameObject.SetActive(false);
        }

        if (timerText != null)
        {
            timerText.gameObject.SetActive(false);
        }
    }

    public void RestartGame()
    {
        gamePaused = false;
        gameEnded = false;
        player1Score = 0;
        player2Score = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void AddScore(int playerNumber, int score)
    {
        if (playerNumber == 1)
        {
            player1Score += score;
        }
        else if (playerNumber == 2)
        {
            player2Score += score;
        }

        UpdateScoreTexts();
    }

    private void PauseGame()
    {
        Time.timeScale = 0f;
        gamePaused = true;
    }

    private void ResumeGame()
    {
        Time.timeScale = 1f;
        gamePaused = false;
    }

    private void UpdateScoreTexts()
    {
        if (player1ScoreText != null)
        {
            player1ScoreText.text = "Puntaje Jugador 1: " + player1Score;
        }

        if (player2ScoreText != null)
        {
            player2ScoreText.text = "Puntaje Jugador 2: " + player2Score;
        }
    }

    private void UpdateTimerText()
    {
        int minutes = Mathf.FloorToInt(timer / 60f);
        int seconds = Mathf.FloorToInt(timer % 60f);
        timerText.text = minutes.ToString("D2") + ":" + seconds.ToString("D2");
    }

    private void EndGame()
    {
        gameEnded = true;
        GameOver();
    }

    public void ResetPlayerPositions()
    {
        player1.transform.position = player1StartPosition;
        player2.transform.position = player2StartPosition;
        spawner.transform.position = spawnerStartPosition;
    }
}